package com.example.practicafoto.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practicafoto.ui.models.RespuestaModel
import com.example.practicafoto.ui.repository.MainRepository
import kotlinx.coroutines.launch
import java.io.File

class ActivityViewModel : ViewModel() {

    //service
    private  var repository = MainRepository()

    val respuestaFoto = MutableLiveData<RespuestaModel>()

    fun sendFoto(file:File){
        Log.d("ViewModel",file.absolutePath)
        viewModelScope.launch {
            val response = repository.sendFoto(file)
            Log.d("ViewModel",response.message())

            if (response.isSuccessful){
                respuestaFoto.postValue(response.body())
            }
        }

    }
}



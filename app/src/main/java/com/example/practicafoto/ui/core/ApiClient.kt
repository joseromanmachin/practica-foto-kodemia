package com.example.practicafoto.ui.core

import com.example.practicafoto.ui.models.RespuestaModel
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiClient {
    //Fotos
    @Headers("Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9kYXRhb25lc29sdXRpb24uZGRucy5uZXQ6ODAwMlwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjE2MjMzNiwiZXhwIjoxNjQ2MTY1OTM2LCJuYmYiOjE2NDYxNjIzMzYsImp0aSI6ImFGMDRRTWpIYml4dGcxd0ciLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.RO1NIiURHBTT43ccnbeh711m6ZYTHLTMGjxVHCn14ls")
    @Multipart
    @POST("/api/users/uploadProfile")
    suspend fun setImagen(@Part("profile\"; filename=\"pp.jpg\" ") archivo: RequestBody): Response<RespuestaModel>

}
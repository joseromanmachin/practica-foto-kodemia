package com.example.practicafoto.ui.repository

import com.example.practicafoto.ui.models.RespuestaModel
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File

class MainRepository {

    private val service = ServiceNetwork()


   suspend fun sendFoto(file:File): Response<RespuestaModel> {
        return service.sendImagen(file)
    }
}
package com.example.practicafoto.ui.repository

import com.example.practicafoto.ui.core.ApiClient
import com.example.practicafoto.ui.core.RetrofitInstance
import com.example.practicafoto.ui.models.RespuestaModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File

class ServiceNetwork {

    private val retrofit = RetrofitInstance.getRetrofit().create(ApiClient::class.java)

    suspend fun sendImagen(file: File): Response<RespuestaModel> {
        val profile = RequestBody.create(MediaType.parse("image/*"),file)

        return withContext(Dispatchers.IO) {
            val response =retrofit.setImagen(profile)
            response

        }

    }
}